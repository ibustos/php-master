<?php

function __autoload($classname) {
  include $classname . '.php';
}


$mono = new Courier('Monospace Delivery');
echo 'Courier name: ' . $mono->getName();


var_dump($mono::getCouriersByCountry('co'));

$courier = new PigeonPost('Local Avian Delivery LTD');

if ($courier instanceOf Courier) {
  echo "{$courier->getName()} is a Courier.<br>";
}
if ($courier instanceOf PigeonPost) {
  echo "{$courier->getName()} is a PigeonPost.<br>";
}

$parcel = new Parcel();
$parcel->setWeight(4)->setCountry('co');

echo ("total:" . $courier->calculateShipping($parcel) . '<br>');


$courier = new Courier('Local Avian Delivery LTD');
$courier->ship($parcel);
$courier->ship(new Parcel());
$courier->ship(new Parcel());

echo (count($courier) . '<br>');


$new_parcel = new Parcel();
$new_parcel->setWeight(6);

try {
  $courier->ship($new_parcel);
}
catch (HeavyParcelException $e) {
  echo ($e->getMessage() . '<br>');
}
