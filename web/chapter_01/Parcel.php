<?php

class Parcel {
  public $weight;
  public $destinationAddress;
  public $destinationCountry;

  public function setWeight($weight) {
    $this->weight = $weight;
    return $this;
  }

  public function setCountry($country) {
    $this->destinationCountry = $country;
    return $this;
  }

}
