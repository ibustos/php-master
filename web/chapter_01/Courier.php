<?php

class Courier implements \Countable {
  protected $name;
  protected $count;

  public function __construct($name) {
    $this->name = $name;
    return TRUE;
  }

  public function getName() {
    return $this->name;
  }

  public function setName($value) {
    $this->name = $value;
  }

  public static function getCouriersByCountry($country) {
    $countries = [
      'co' => [
        'Servientrega',
        'Deprisa',
        '4-72',
      ],
      'ec' => [
        'ups',
        'fedex',
      ],
    ];
    return $countries[$country];
  }

  private function getShippingRateForCountry($country) {
    $rates = [
      'co' => 1.13,
      'ec' => 2.10,
    ];
    return $rates[$country];
  }

  public function calculateShipping(Parcel $parcel) {
    $rate = $this->getShippingRateForCountry($parcel->destinationCountry);

    $cost = $rate * ($parcel->weight);
    return $cost;
  }

  public function ship(Parcel $parcel) {
    if ($parcel->weight > 5) {
      throw new HeavyParcelException('Parcel exceeds courier limit.');
    }
    // Send parcel to destination.
    $this->count++;
    return TRUE;
  }

  public function count() {
    return $this->count;
  }

}
